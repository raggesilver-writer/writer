/* preview.vala
 *
 * Copyright 2019 Paulo Queiroz <pvaqueiroz@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

public class Writer.Preview : WebKit.WebView {

    string css_base = """
        img { max-width: 100%; }
        content {
            margin: 2em auto;
            font-family: 'Avenir', Helvetica, Arial, sans-serif;
        }
        body { margin: 0 3em; }
    """;
    public string saved_content = "";
    Gtk.ApplicationWindow window;

    public Preview(string content, Gtk.ApplicationWindow window) {
        this.window = window;

        render(content);
    }

    private string apply_template(string content) {

        Gdk.RGBA bg, fg;
        var ctx = window.get_style_context();

        ctx.lookup_color("theme_base_color", out bg);
        ctx.lookup_color("theme_fg_color", out fg);

        var css = @"$css_base body { background: $(bg.to_string()); color: $(fg.to_string()); }";

        string res = get_string_from_resource("layouts/preview.html");
        res = res.printf(css, content);
        return (res);
    }

    public void render(string a) {
        var md = Markdown.parse(a.data);
        var body = md.render_html();

        load_html(apply_template(body), null);
    }

    protected override
    bool decide_policy(WebKit.PolicyDecision dc, WebKit.PolicyDecisionType tp) {

        if (tp == WebKit.PolicyDecisionType.RESPONSE) {
            var rpd = dc as WebKit.ResponsePolicyDecision;
            xdg_open(rpd.response.uri);
            dc.ignore();
            return false;
        }

        return true;
    }
}
