/* publish_window.vala
 *
 * Copyright 2019 Paulo Queiroz <pvaqueiroz@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

[GtkTemplate (ui="/com/raggesilver/Writer/layouts/publish_window.ui")]
public class Writer.PublishWindow : Gtk.ApplicationWindow {

    public Proton.File file { get; set; }
    public string content { get; set; }
    AsyncButton publish_button;

    [GtkChild]
    Gtk.HeaderBar header_bar;

    [GtkChild]
    Gtk.Button cancel_button;

    [GtkChild]
    Gtk.Revealer error_revealer;

    [GtkChild]
    Gtk.Label error_label;

    [GtkChild]
    Gtk.Entry title_entry;

    public PublishWindow(Gtk.ApplicationWindow parent,
                         Proton.File f,
                         string content) {
        Object(application: parent.get_application(),
               file: f,
               content: content);
        set_transient_for(parent);

        publish_button = new AsyncButton("Publish");
        publish_button.get_style_context().add_class("suggested-action");

        header_bar.pack_end(publish_button);

        string? title = get_article_title();
        if (title != null)
            title_entry.text = title.offset(2);

        cancel_button.clicked.connect(() => {
            destroy();
        });

        publish_button.clicked.connect(() => {
            publish.begin();
        });
    }

    async void publish() {
        string? token = null;

        if (!signed || (token = yield guard.get_token()) == null) {
            error_label.set_text("Not logged in.");
            error_revealer.set_reveal_child(true);

            var lw = new LoginWindow(this);
            lw.show();

            return ; // TODO call login from here
        }

        error_revealer.set_reveal_child(false);
        publish_button.set_is_working(true);

        HashTable<string, string> payload =
            new HashTable<string, string>(str_hash, str_equal);

        payload.set("content", content);
        payload.set("fileid", settings.email + file.path);
        payload.set("title", title_entry.get_text());

        string req = hash_table_to_json_string(payload);
        print("Request: " + req + "\n");

        axios.token = token;
        axios.post(@"$API_BASE/publish", req, (ses, res) => {
            string response = (string) res.response_body.data;
            print(@"$response\n");
            if (res.status_code == 200 || res.status_code == 201) {
                xdg_open(API_BASE.replace("/api", "") + @"/view/$response");
                destroy();
                return ;
            } else {
                error_label.set_text(response);
                error_revealer.set_reveal_child(true);
            }
            publish_button.set_is_working(false);
        });
    }

    string? get_article_title() {
        Regex r = /# .+/;
        MatchInfo mi;

        if (r.match(content, 0, out mi))
            return mi.fetch_all()[0];

        return null;
    }
}
