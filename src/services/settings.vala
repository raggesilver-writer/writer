public class Writer.Settings : Granite.Services.Settings {

    private static Writer.Settings? instance = null;

    public int window_width { get; set; }
    public int window_height { get; set; }
    public int preview_position { get; set; }
    public int window_x { get; set; }
    public int window_y { get; set; }
    public bool dark_mode { get; set; }
    public string[] recent { get; set; }
    public bool highlight { get; set; }
    public bool preview { get; set; }
    public string name { get; set; }
    public string email { get; set; }

    private Settings() {
        base("com.raggesilver.Writer");
        if (name == null || name == "") {
            try_get_name();
        }
        if (email == null || email == "") {
            try_get_email();
        }
    }

    private void try_get_name() {
        string name, err;

        if(Writer.Shell.get_instance().run(
            "git config user.name", out name, out err) == 0) {
            this.name = name.strip();
        }
    }

    private void try_get_email() {
        string email, err;

        if(Writer.Shell.get_instance().run(
            "git config user.email", out email, out err) == 0) {
            this.email = email.strip();
        }
    }

    public static Writer.Settings get_instance() {
        if (instance == null)
            instance = new Writer.Settings ();
        return instance;
    }

    public void add_recent(string path) {
        string[] _recent = {};
        _recent += path;
        foreach (var item in recent) {
            if (!(item in _recent))
                _recent += item;
            if (_recent.length >= 5)
                break ;
        }
        recent = _recent;
    }
}
