public class Writer.Guard : Object {
    static Guard instance = null;

    private Secret.Schema schema;
    private GLib.HashTable<string, string> attr;

    private Guard() {
        schema = new Secret.Schema("com.raggesilver.Writer",
                                   Secret.SchemaFlags.NONE,
                                   "number",
                                   Secret.SchemaAttributeType.INTEGER,
                                   "string",
                                   Secret.SchemaAttributeType.STRING,
                                   "even",
                                   Secret.SchemaAttributeType.BOOLEAN);
        attr = new GLib.HashTable<string, string>(null, null);
        attr["number"] = "20";
        attr["string"] = "labatata";
        attr["even"] = "false";

        axios.error.connect((msg) => {
            if (msg.status_code == 401) {
                signed = false;
                remove_token.begin();
            }
        });
    }

    public async string? get_token() {
        try {
            string password = yield Secret.password_lookupv(schema, attr, null);
            return (password);
        } catch (GLib.Error e) {
            warning(e.message);
            return (null);
        }
    }

    public async void validate_token() {
        axios.token = yield get_token();
        signed = axios.token != null;

        if (!signed)
            return ;

        var res = yield axios.post_async(@"$API_BASE/auth/validate", "");

        /**
         * If the server was contacted and the token is invalid
         * then log the user out. This avoids removing the credentials
         * in case there is a communication error
         */
        if (res.status_code == 401) {
            signed = false;
            yield remove_token();
        }
    }

    public async bool set_token(string token) {
        try {
            bool res = yield Secret.password_storev(schema, attr,
                Secret.COLLECTION_DEFAULT, "token", token, null);
            Writer.signed = res;
            return (res);
        } catch (GLib.Error e) {
            warning(e.message);
            return (false);
        }
    }

    public async bool remove_token() {
        if ((yield get_token()) == null)
            return (true);
        try {
            bool res = yield Secret.password_clearv(schema, attr, null);
            return (res);
        } catch (GLib.Error e) {
            warning(e.message);
            return (false);
        }
    }

    public static Guard get_instance() {
        if (instance == null)
            instance = new Guard();
        return (instance);
    }
}
