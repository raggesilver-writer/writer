/* utils.vala
 *
 * Copyright 2019 Paulo Queiroz <pvaqueiroz@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

namespace Writer {

    const string RES_BASE = "/com/raggesilver/Writer";

    string hash_table_to_json_string(HashTable<string, string> table) {
        string res = "";

        table.foreach((_k, _v) => {
            string k = (_k.to_ascii()).escape(); // Fixes #6
            string v = (_v.to_ascii()).escape(); //
            res += @"\"$k\":\"$v\",";
        });

        if (res.length > 1)
            res = res.substring(0, res.length - 1); // remove the last comma
        res = "{" + res + "}";
        return (res);
    }

    int ceil(float n) {
        int res;

        res = (int) n;
        res = (((int) res < (float) n) ? res + 1 : res);
        return (res);
    }

    void xdg_open(string uri) {
        try {
            AppInfo.launch_default_for_uri(uri, null);
        } catch(Error e) {
            warning(e.message);
        }
    }

    string get_string_from_resource(string resource) {
        try {
            Bytes res = resources_lookup_data(@"$RES_BASE/$resource",
                ResourceLookupFlags.NONE);
            return ((string) res.get_data());
        } catch (Error e) {
            warning(e.message);
            return "";
        }
    }
}
