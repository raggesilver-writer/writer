/* window.vala
 *
 * Copyright 2019 Paulo Queiroz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

[GtkTemplate (ui = "/com/raggesilver/Writer/layouts/window.ui")]
public class Writer.WelcomeWindow : Gtk.ApplicationWindow {

    Gtk.Settings s;

    [GtkChild]
    Gtk.Box recent_articles_box;

    [GtkChild]
    Gtk.Label user_name_label;

    [GtkChild]
    Gtk.Label user_email_label;

    [GtkChild]
    Gtk.Button open_article_button;

    [GtkChild]
    Gtk.Button open_login_dialog_button;

    [GtkChild]
    Gtk.Button profile_menu_button;

    [GtkChild]
    Gtk.Button new_article_button;

    [GtkChild]
    Gtk.Button do_logout_button;

    public WelcomeWindow (Gtk.Application app) {
        Object (application: app);

        get_signed.begin();

        if (settings.window_width > 0 && settings.window_height > 0)
            resize(settings.window_width, settings.window_height);
        if (settings.window_x >= 0 && settings.window_y >= 0)
            move(settings.window_x, settings.window_y);

        delete_event.connect((e) => {
            return on_delete();
        });

        s = Gtk.Settings.get_default();
        s.gtk_application_prefer_dark_theme = Writer.settings.dark_mode;

        var css_provider = new Gtk.CssProvider();
        css_provider.load_from_resource(
            "/com/raggesilver/Writer/resources/style.css");

        Gtk.StyleContext.add_provider_for_screen (Gdk.Screen.get_default(),
            css_provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);

        do_logout_button.clicked.connect(() => {
            do_logout_button.set_sensitive(false);
            guard.remove_token.begin((obj, res) => {
                if (guard.remove_token.end(res)) {
                    Writer.signed = false;
                    udpate_signed_ui();
                }
                do_logout_button.set_sensitive(true);
            });
        });

        open_login_dialog_button.clicked.connect(() => {
            var w = new LoginWindow(this);
            w.response.connect(udpate_signed_ui);
            w.show();
        });

        new_article_button.clicked.connect(() => {
            open_writing_window();
        });

        open_article_button.clicked.connect(open);

        load_recent();
    }

    bool on_delete() {

        int x, y, width, height;

        get_size(out width, out height);
        get_position(out x, out y);

        settings.window_width = width;
        settings.window_height = height;
        settings.window_x = x;
        settings.window_y = y;

        return false;
    }

    Gtk.ModelButton recent_button(string path) {
        var mb = new Gtk.ModelButton();
        var lbl = new Gtk.Label(path);
        lbl.width_chars = 35;
        lbl.max_width_chars = 50;
        lbl.set_ellipsize(Pango.EllipsizeMode.START);
        lbl.set_xalign(0);
        lbl.set_sensitive(false);
        (mb.get_child() as Gtk.Box).pack_start(lbl, false, false, 0);

        mb.show_all();
        return (mb);
    }

    int max(int n, int max) {
        return ((n > max) ? max : n);
    }

    void load_recent() {
        string[] recent = Writer.settings.recent;
        Proton.File[] recents = {};

        /**
         * This prevents adding buttons for files that no longer exist
         */
        foreach (string s in recent) {
            var f = new Proton.File(s);
                if (f.exists)
                    recents += f;
        }

        if (recent.length > 0) {
            recent_articles_box.get_parent().get_parent()
                .set_size_request(0, max(recent.length * 30, 70));

            foreach (var f in recents) {
                var mb = recent_button(f.path);
                recent_articles_box.pack_start(mb, false, false, 0);

                mb.clicked.connect(() => {
                    open_writing_window(f);
                });
            }
        } else {
            recent_articles_box.pack_start(new Gtk.Label("No recent articles"),
                                           true, false, 0);
            recent_articles_box.show_all();
        }
    }

    void open() {
        var dialog = new Gtk.FileChooserDialog("Open article", this,
                Gtk.FileChooserAction.OPEN,
                "Cancel", Gtk.ResponseType.CANCEL,
                "_Open", Gtk.ResponseType.OK,
                null);

        var filter = new Gtk.FileFilter();
        filter.add_pattern("*.md");
        dialog.set_filter(filter);

        if (dialog.run() == Gtk.ResponseType.OK) {
            var f = new Proton.File(dialog.get_filename());
            dialog.destroy();
            open_writing_window(f);
            return ;
        }

        dialog.destroy();
    }

    public void udpate_signed_ui() {
        if (signed) {
            open_login_dialog_button.hide();
            profile_menu_button.show();

            user_name_label.show();
            user_email_label.show();

            user_name_label.set_text(settings.name);
            user_email_label.set_text(settings.email);
        } else {
            profile_menu_button.hide();
            open_login_dialog_button.show();
            user_name_label.hide();
            user_email_label.hide();
        }
    }

    async void get_signed() {
        yield guard.validate_token();
        udpate_signed_ui();
    }

    void open_writing_window(Proton.File? file = null) {
        var mw = new Writer.MainWindow(this.get_application(), file);
        mw.show();
        this.close();
    }
}
