# Writer

Simple markdown editor connected to a publishing API that I'm writing myself.
Check out the "blog" where publications made with Writer are presented: [https://ragge.site](https://ragge.site)

![Preview](https://gitlab.com/raggesilver-writer/writer/raw/master/preview.png)

**Important note:** Writer is still in **development stage**. Writer's server is twice as much in development stage and it's **database might be reset** several times before it reaches production stage!

---

## Installing
**Meson**
```bash
meson _build
cd _build
ninja
ninja install # You need sudo for this
```
**Flatpak**

Build a flatpak using `flatpak-builder` (gnome-builder makes that very easy)

## Support
<a href="https://www.patreon.com/bePatron?u=18325836" data-patreon-widget-type="become-patron-button">Become a Patron!</a>

## Todo
- Image insertion tool: upload a local image to a cloud system (like imgur) and insert the URL in the article
    - This could also be expanded for any kind of media (Google Drive is an upload option here)
- Make open article after publishing an option

## Notes
`New article` in the dropdown menu is disabled by default but might be enabled if a local variable:
```bash
MULTIPLE=true ./writer
```
You can clone the server from [writer-server](https://gitlab.com/raggesilver-writer/writer-server) and run a local instance for development. To use the Writer application with a local server just change the `API_BASE` varialbe
```bash
API_BASE="http://localhost:3000/api" ./path/to/writer
```

### Tests

Doing math in markdown: h<sub>&theta;</sub>(x) = &theta;<sub>o</sub> x + &theta;<sub>1</sub>x

#Linux #Writer
